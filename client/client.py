import requests

server_url = 'http://server:5000/api/data'


def get_data():
    response = requests.get(server_url)
    if response.status_code == 200:
        print('Data received from server:', response.json())
    else:
        print('Failed to get data from server')


def post_data(data):
    response = requests.post(server_url, json=data)
    if response.status_code == 200:
        print('Data sent to server successfully:', response.json())
    else:
        print('Failed to send data to server')


def put_data(data):
    response = requests.put(server_url, json=data)
    if response.status_code == 200:
        print('Data updated on server successfully:', response.json())
    else:
        print('Failed to update data on server')


def delete_data(data):
    response = requests.delete(server_url, json=data)
    if response.status_code == 200:
        print('Data deleted on server successfully:', response.json())
    else:
        print('Failed to delete data on server')


if __name__ == '__main__':
    get_data()

    data_to_send = {'number': 6}
    post_data(data_to_send)

    update_message = {'message': 'Updated message from the client!'}
    put_data(update_message)

    data_to_delete = {'number': 3}
    delete_data(data_to_delete)

    get_data()
