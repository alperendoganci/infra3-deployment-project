#!/bin/bash

# Create a Docker network
function network_exists {
    docker network ls | grep -q "$1"
}

# Create a Docker network if it doesn't exist
network_name="infra3-deployment-project-network"
if ! network_exists "$network_name"; then
    echo "Creating Docker network: $network_name"
    docker network create "$network_name"
else
    echo "Docker network $network_name already exists"
fi


# Build server container
echo "Building server container..."
docker build -t infra3-deployment-project-server ./server

# Build client container
echo "Building client container..."
docker build -t infra3-deployment-project-client ./client

# Run server container
echo "Running server container..."
docker run -d --name infra3-deployment-project-server --network "$network_name" -p 5000:5000 infra3-deployment-project-server

# Run client container
echo "Running client container"
docker run -d --name infra3-deployment-project-client --network "$network_name" infra3-deployment-project-client

# Wait for containers to initialize
sleep 5

# Send a GET request to the server
echo "Sending GET request to the server..."
curl -s http://localhost:5000/api/data

# Send a POST request to the server
echo "Sending POST request to the server..."
curl -s -X POST -H "Content-Type: application/json" -d '{"name": "Client", "number": [6]}' http://localhost:5000/api/data

echo "Deployment completed successfully"

