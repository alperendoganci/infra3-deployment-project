#!/bin/bash


# Check if the network exists, create it if it doesn't
if ! docker network ls | grep -q 'infra3-deployment-project-network'; then
    echo "Creating Docker network infra3-deployment-project-network..."
    docker network create infra3-deployment-project-network
else
    echo "Docker network infra3-deployment-project-network already exists"
fi


# Check if docker compose is available
if ! command -v docker compose &> /dev/null
then
    echo "docker compose could not be found in the PATH. Please ensure it is installed and in the PATH."
    exit 1
fi

# Build and start the containers using Docker Compose
docker compose -f docker-compose.yml up --build -d

# Wait for containers to initialize
sleep 5

# Send a GET request to the server
echo "Sending GET request to the server..."
curl -s http://localhost:5000/api/data

# Send a POST request to the server
echo "Sending POST request to the server..."
curl -s -X POST -H "Content-Type: application/json" -d '{"name": "Client", "number": [6]}' http://localhost:5000/api/data

echo "Deployment completed successfully"