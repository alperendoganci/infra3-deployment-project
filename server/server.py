from flask import Flask, jsonify, request

app = Flask(__name__)

# Simple in-memory data store
data_store = {
    'message': 'Hello from the server!',
    'data': [1, 2, 3, 4, 5]
}


@app.route('/api/data', methods=['GET'])
def get_data():
    return jsonify(data_store)


@app.route('/api/data', methods=['POST'])
def post_data():
    new_data = request.json
    data_store['data'].append(new_data['number'])
    return jsonify({'status': 'success', 'new_data': data_store})


@app.route('/api/data', methods=['PUT'])
def put_data():
    update_data = request.json
    data_store['message'] = update_data['message']
    return jsonify({'status': 'success', 'updated_data': data_store})


@app.route('/api/data', methods=['DELETE'])
def delete_data():
    number_to_delete = request.json['number']
    if number_to_delete in data_store['data']:
        data_store['data'].remove(number_to_delete)
        return jsonify({'status': 'success', 'remaining_data': data_store})
    else:
        return jsonify({'status': 'failure', 'message': 'Number not found in data'})


if __name__ == '__main__':
    app.run(debug=True, port=5000, host='0.0.0.0')
